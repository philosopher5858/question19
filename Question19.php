

<!DOCTYPE html>
<html lang="en">
<head>
   <style>
 .header{
   position:relative;
   width: 510px;
   height: 105px;
   border-radius: 10px;
   padding: 10px;
   bottom: 50%;
   font-size: 17px;
   box-sizing: border-box;
   background:#303940;
   box-shadow: 1px 1px 4px #d0c1cd, -1px -1px 4px white;
   letter-spacing: 2px;
   color: #f7f9fb;
   
 }
 
 body {
   margin: 0;
   width: 100vw;
   height: 100vh;
   background:linear-gradient(#303940,gray);
   display: flex;
   align-items: center;
   text-align: center;
   justify-content: center;
   place-items: center;
   overflow: hidden;
   font-family: 'Courier New', Courier, monospace;
 }
 
 .container {
   position: relative;
   width: 500px;
   height: 180px;
   border-radius: 20px;
   padding: 40px;
   top: 55%;
   box-sizing: border-box;
   background:#303940;
   box-shadow: 1px 1px 4px #cbced1, -1px -1px 4px white;
   letter-spacing: 4px;
   color: white;
   
 }
 .container2{
    position:absolute;
   width: 700px;
   height: 600px;
   border-radius: 10px;
   padding: 250px 100px;
   font-size: 17px;
   box-sizing: border-box;
   background:#303940;
   box-shadow: 1px 1px 2px #d0c1cd, -1px -1px 2px white;
   letter-spacing: 2px;
   color: #f7f9fb;
 }
 
 .inputs {
   text-align: left;
   margin-top: 3px;
 }
 
 label, input, button {
   display:inline;
   width: 100%;
   padding: 0;
   border: none;
   outline: none;
   box-sizing: border-box;
 }
 .lable2{
   margin-bottom: 40px;
   padding-right: 100%;
   font-family: 'Courier New', Courier, monospace;
 }
 
 label {
   margin-bottom: 40px;
   padding-right: 100%;
   font-family: 'Courier New', Courier, monospace;
 }
 
 label:nth-of-type(2) {
   margin-top: 12px;
   padding-right: 0%;
 }
 
 input::placeholder {
   color: gray;
 }
 
 input {
   background: #f7f9fb;
   padding: 5px;
   padding-left: 20px;
   height: 40px;
   font-size: 14px;
   border-style:inset;
   border-radius: 10px;
   box-shadow: inset 2px 1px 2px #535c65, inset -2px -1px 2px rgb(69, 78, 79);
 }
 
 button {
   color: white;
   margin-top: 20px;
   background: #303940;
   height: 40px;
   width: 220px;
   border-radius: 10px;
   cursor: pointer;
   font-weight: 900;
   box-shadow: 1px 1px 1px #cbced1, -1px -1px 1px white;
   transition: 0.5s;
 }
 
 button:hover {
   box-shadow: none;
 }
 
 a {
   position: absolute;
   font-size: 8px;
   bottom: 4px;
   right: 4px;
   text-decoration: none;
   color: black;
   background: yellow;
   border-radius: 10px;
   padding: 2px;
 }
 
 h1 {
   position: absolute;
   top: 0;
   left: 0;
 }
 
   </style>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
</head>
<body>
    <div class="container2">
    <div class="header">
        Question 19. <br> WAP to enter the %age of a student & print whether he is PASS or FAIL.
   </div>
   <form action="Question19.html" method="POST">
   
       <DIv class="container">
                <?php
                if($_SERVER['REQUEST_METHOD']=='POST'){

                    $mark = $_POST['mark'];

                    print "INPUT: ".$mark."<br><br>";
                    print "OUTPUT: "."<BR>";

                    if($mark >= 40 && $mark <= 100){
                        print "PASS"."<br>";
                    }elseif($mark >= 0 && $mark <= 40){
                        print "FAIL"."<br>";
                    }else{
                        print "Wrong Mark Entered"."<br>";
                    }
                }

                ?>
              
                       <button type="submit">GO BACK</button>
                
       </DIv>
   </form>
   </div>
</body>
</html>